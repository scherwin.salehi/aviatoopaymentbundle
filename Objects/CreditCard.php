<?php

namespace Aviatoo\Payment\Objects;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Aviatoo\Rest\Constants\GroupConstants as G;
use JMS\Serializer\Annotation\Groups;

/**
 *
 *
 */
class CreditCard
{

    const NEW_CREDIT = "NEW_CREDIT";

    /**
     * @Assert\NotBlank(groups={CreditCard::NEW_CREDIT})
     * @Assert\Regex(
     *     pattern="/^[0-9]{3}$/",
     *     groups={G::EDIT,CreditCard::NEW_CREDIT}
     * )
     * @Groups({G::EDIT,CreditCard::NEW_CREDIT,G::ENTITY_OUT})
     * @Serializer\Type("integer")
     * @var $cvc integer
     */
    private $cvc;

    /**
     * @Assert\Type("int")
     * @Assert\NotBlank(groups={CreditCard::NEW_CREDIT})
     * @Assert\Length(
     *     min="12",
     *     max="16",
     *     groups={G::EDIT,CreditCard::NEW_CREDIT},
     * )
     * @Groups({G::EDIT,CreditCard::NEW_CREDIT,G::ENTITY_OUT})
     * @Serializer\Type("integer")
     * @var $number integer
     */
    private $number;


    /**
     * @Assert\Type("int")
     * @Assert\NotBlank(groups={CreditCard::NEW_CREDIT})
     * @Assert\GreaterThanOrEqual(value="1",groups={CreditCard::NEW_CREDIT})
     * @Assert\LessThanOrEqual(value="12",groups={CreditCard::NEW_CREDIT})
     * @Groups({CreditCard::NEW_CREDIT,G::ENTITY_OUT})
     * @Serializer\Type("integer")
     * @var int $expMonth
     */
    private $expMonth;

    /**
     * @Assert\Type("int")
     * @Assert\NotBlank(groups={CreditCard::NEW_CREDIT})
     * @Assert\GreaterThanOrEqual(value="2019",groups={CreditCard::NEW_CREDIT})
     * @Assert\LessThanOrEqual(value="2200",groups={CreditCard::NEW_CREDIT})
     * @Groups({CreditCard::NEW_CREDIT,G::ENTITY_OUT})
     * @Serializer\Type("integer")
     * @var int $expMonth
     */
    private $expYear;

    /**
     * @return int
     */
    public function getCvc()
    {
        return $this->cvc;
    }

    /**
     * @param int $cvc
     */
    public function setCvc(int $cvc)
    {
        $this->cvc = $cvc;
    }

    /**
     * @return int
     */
    public function getExpMonth()
    {
        return $this->expMonth;
    }

    /**
     * @param int $expMonth
     */
    public function setExpMonth(int $expMonth)
    {
        $this->expMonth = $expMonth;
    }

    /**
     * @return int
     */
    public function getExpYear()
    {
        return $this->expYear;
    }

    /**
     * @param int $expYear
     */
    public function setExpYear(int $expYear)
    {
        $this->expYear = $expYear;
    }


    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }



    public function toArray(){
        return [
            "number"=>$this->number,
            "exp_month"=>$this->expMonth,
            "exp_year"=>$this->expYear,
            "cvc"=>$this->cvc
        ];
    }


}